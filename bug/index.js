'use strict';
var util        = require('util'),
    yeoman      = require('yeoman-generator'),
    fs          = require('fs'),
    path        = require('path'),
    bz          = require('bz'),
    Q           = require('q'),
    util        = require('util'),
    request     = require('request'),
    async       = require('async');

var BugzillaGenerator = module.exports = yeoman.Base.extend({
  constructor: function (args, options, config) {
      yeoman.Base.apply(this, arguments);
      this.appName = this.options.appName;
      var _this = this;
      var done = this.async();

      _this.readFileAsString = require("html-wiring").readFileAsString;
      try {
        var boptions = fs.readFileSync(path.resolve(process.env.YOBUGZY), 'utf8');
        _this.bugzilla_options = JSON.parse(boptions);
      } catch(err) {
        _this.env.error("YOBUGZY environment variable must point to a JSON file with yobugzy options");
      }
  },
  end: function () {
    console.log( util.format("New BUG created #%s", this.new_issue_id) );
  },
  prompting: function() {
    var _this = this;
    var cb = _this.async();

    _this.prompts = [];

    _this.bugzilla = bz.createClient({
      url: _this.bugzilla_options.url || "http://localhost/bugzilla/rest/",
      username: _this.bugzilla_options.credentials.username ,
      password: _this.bugzilla_options.credentials.password,
      timeout: _this.bugzilla_options.timeout || 30000
    });
    _this.bugzilla.getProducts("accessible")
      .then(function onResolved(products) {
        _this.products = [];
        for( var p in products.products ) {
          _this.products.push( { name: products.products[p].name, components: products.products[p].components, versions: products.products[p].versions });
        }

        _this.prompts.push({
          type: 'list',
          name: 'product',
          message: 'What PRODUCT are you filing for?',
          choices: function() {
            var p = [];
            for( var product in _this.products ) {
              p.push(_this.products[product].name);
            }
            return p;
          },
          default: 0
        });

        _this.prompts.push({
          type: 'list',
          name: 'component',
          message: 'What COMPONENT is this related to?',
          choices: function(answers) {
            var components = [];
            _this.products.forEach( function(currentValue, index, array) {
              if( array[index].name === answers.product ) {
                for( var c in array[index].components ) {
                  components.push( array[index].components[c].name );
                }
              }
            });
            return components;
          },
          default: 0
        });

        _this.prompts.push({
          type: 'input',
          name: 'summary',
          message: 'First, just a SUMMARY please.',
          validate: function validateProject(value) {
            return value === "" ? "Summary is required" : true;
          }
        });

        _this.prompts.push({
          type: 'input',
          name: 'observation',
          message: 'Please tell me what happened.',
          validate: function validateObservation(value) {
            return value === "" ? "Observation is required" : true;
          }
        });

        _this.prompts.push({
          type: 'input',
          name: 'expectation',
          message: 'What did you expect to happen?',
          validate: function validateExpectation(value) {
            return value === "" ? "Expectation is required" : true;
          }
        });

        _this.prompts.push({
          type: 'list',
          name: 'platform',
          message: 'What PLATFORM did you see this on?',
          choices: _this.bugzilla_options.platforms,
          default: 0
        });

        _this.prompts.push({
          type: 'list',
          name: 'priority',
          message: 'How important is it?',
          choices: _this.bugzilla_options.priorities, 
          default: 2
        });

        _this.prompts.push({
          type: 'list',
          name: 'severity',
          message: 'How severe is it?',
          choices: _this.bugzilla_options.severities, 
          default: 3
        });

        _this.prompts.push({
          type: 'list',
          name: 'op_sys',
          message: 'What OS did you see this on?',
          choices: _this.bugzilla_options.operating_systems, 
          default: 0
        });

        _this.prompts.push({
          type: 'list',
          name: 'version',
          message: 'On what version of the software did you notice this?',
          choices: function(answers) {
            var versions = [];
            _this.products.forEach( function(currentValue, index, array) {
              if( array[index].name === answers.product ) {
                for( var v in array[index].versions ) {
                  versions.push( array[index].versions[v].name )
                }
              }
            });
            return versions;
          }, 
          default: 0
        });

        _this.prompt(_this.prompts, function (response) {
          _this.product = response.product;
          _this.summary = response.summary;
          _this.description = util.format("Observation:\n%s\n\nExpectation:\n%s\n\n", response.observation, response.expectation);
          _this.priority = response.priority;
          _this.platform = response.platform;
          _this.component = response.component;
          _this.version = response.version;
          _this.op_sys = response.op_sys;
          _this.severity = response.severity;

          var newBug = {
            product: _this.product,
            summary: _this.summary,
            description: _this.description,
            creator: {
              email: _this.bugzilla_options.credentials.user
            },
            priority: _this.priority,
            component: _this.component,
            op_sys: _this.op_sys,
            version: _this.version,
            severity: _this.severity,
            platform: _this.platform
          };
          _this.bugzilla.createBug(newBug , function(err, bugzilla_id){
            if(err) {
              console.log(util.format("%j", err));
              cb(err);
            } else {
              _this.new_issue_id = bugzilla_id;
              cb();
            }
          });
        }.bind(_this));
      });
  }
});

BugzillaGenerator.prototype.app = function projectFiles() {
};
