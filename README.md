# generator-bugzy
yeoman generator for Bugzilla issues

## Installing
Install the generator globally from npm
```
$ npm install -g generator-bugzy
```
Create bugzy.json by copying the default bugzy.json and editing to match your server

## Usage
There must be a YOBUGZY environment variable set that points to the JSON file with bugzy options. The generator will dynamically query the server for products and components. platforms, priorities, severities and operating systems must all be set in bugzy.json

```
$ YOBUGZY=~/bugzy.json yo bugzy
? What PRODUCT are you filing for? TestProduct
? What COMPONENT is this related to? TestComponent
? First, just a SUMMARY please. a problem happened
? Please tell me what happened. a problem
? What did you expect to happen? not a problem
? What PLATFORM did you see this on? All
? How important is it? Normal
? How severe is it? normal
? What OS did you see this on? All
? On what version of the software did you notice this? 1.0.0
New BUG created #14
```

## bugzy.json
The repo contains default values for the bugzilla server running locally at the /bugzilla/ endpoint, a 30 second timeout, and the credentials for a user. It also has the default bug priorities, severities, operating systems, and platforms. If any of these have been customized on your system, you need to edit bugzy.json with matching values.

```
{
  "url": "http://localhost/bugzilla/rest/",
  "timeout": 30000,
  "credentials": {
    "username": "aumkara@gmail.com",
    "password": "changeme"
  },
  "priorities": [ "Highest", "High", "Normal", "Low", "Lowest", "---" ],
  "severities": [ "blocker", "critical", "major", "normal", "minor", "trivial", "enhancement" ],
  "operating_systems": [ "All", "Windows", "Mac OS", "Linux", "Other" ],
  "platforms": [ "All", "PC", "Macintosh", "Other" ]
}
```